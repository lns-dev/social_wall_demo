<?php
/**
 * @file
 * Enables modules and site configuration for a minimal site installation.
 */

/**
 * Implements hook_install_tasks().
 */
function sw_demo_install_tasks($install_state) {

  $tasks['sw_demo_configuration_tasks'] = array(
    'display_name' => st('Configuration tasks'),
    'display' => FALSE,
    'type' => 'normal',
    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
  );

  return $tasks;
}

function sw_demo_configuration_tasks() {

  $enable = array(
    'theme_default' => 'social_wall_theme',
    'admin_theme' => 'seven',
  );

  theme_enable($enable);

  foreach ($enable as $var => $theme) {
    if (!is_numeric($var)) {
      variable_set($var, $theme);
    }
  }
}

