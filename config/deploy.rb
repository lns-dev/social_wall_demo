# config valid only for current version of Capistrano
lock '3.3.5'

set :application, 'socialwalldemo'
set :repo_url, 'git@github.com:LaNetscouade/social_wall_demo.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('sites/default/settings.php')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('sites/default/files')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do
    after :starting, 'composer:install_executable'
end

namespace :deploy do
    after :updating, :drupal_make do
        on roles(:web) do
            execute "cd #{release_path} && drush make social_wall_demo.make -y"
        end
    end
end

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
