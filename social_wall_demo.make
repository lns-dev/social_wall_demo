; This file was auto-generated by drush make
core = 7.x
api = 2

; Core
projects[drupal][version] = "7.38"

; Modules
projects[imagecache_external][version] = "2.1"
projects[admin_menu][version] = "3.0-rc5"
projects[libraries][version] = "2.2"

; Themes
projects[bootstrap][version] = "3.0"

; Modules
projects[social_wall][type] = "module"
projects[social_wall][download][type] = "git"
projects[social_wall][download][url] = "git@github.com:LaNetscouade/social_wall.git"
projects[social_wall][download][branch] = master

; Libraries
; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[salvattore][download][type] = "get"
libraries[salvattore][download][url] = "https://github.com/rnmp/salvattore/archive/v1.0.7.zip"
libraries[salvattore][directory_name] = "salvattore"
libraries[salvattore][type] = "library"

